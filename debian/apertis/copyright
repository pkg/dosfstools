Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: no-info-found
License: GPL-3+

Files: aclocal.m4
 config.rpath
Copyright: 1996-2018, Free Software Foundation, Inc.
License: FSFULLR

Files: compile
 depcomp
 missing
 test-driver
Copyright: 1996-2018, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL

Files: configure.ac
Copyright: 2015, 2016, Andreas Bombe <aeb@debian.org>
License: GPL-3+

Files: debian/*
Copyright: 2017-2021, Pali Rohár <pali.rohar@gmail.com>
 2015-2017, Andreas Bombe <aeb@debian.org>
 2008-2014, Daniel Baumann <mail@daniel-baumann.ch>
 1999-2005, Werner Almesberger <werner.almesberger@lrc.di.epfl.ch>
 1999-2005, Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>
 1993, 1994, David Hudson <dave@humbug.demon.co.uk>
 1992, 1993, Remy Card <card@masi.ibp.fr>
 1991, Linus Torvalds <torvalds@klaava.helsinki.fi>
License: GPL-3+

Files: install-sh
Copyright: 1994, X Consortium
License: X11

Files: manpages/*
Copyright: 2017-2021, Pali Rohár <pali.rohar@gmail.com>
 2016, 2017, Andreas Bombe <aeb@debian.org>
 2006-2014, Daniel Baumann <daniel@debian.org>
License: GPL-3+

Files: manpages/Makefile.am
Copyright: 2015, 2016, Andreas Bombe <aeb@debian.org>
License: GPL-3+

Files: manpages/Makefile.in
Copyright: 1994-2018, Free Software Foundation, Inc.
License: GPL-3+

Files: src/Makefile.am
 src/testdevinfo.c
Copyright: 2015, 2016, Andreas Bombe <aeb@debian.org>
License: GPL-3+

Files: src/Makefile.in
Copyright: 1994-2018, Free Software Foundation, Inc.
License: GPL-3+

Files: src/blkdev/blkdev.c
 src/blkdev/blkdev.h
Copyright: no-info-found
License: public-domain

Files: src/boot.c
 src/check.c
Copyright: 2017-2021, Pali Rohár <pali.rohar@gmail.com>
 2015-2017, Andreas Bombe <aeb@debian.org>
 2008-2014, Daniel Baumann <mail@daniel-baumann.ch>
 1998, Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>
 1993, Werner Almesberger <werner.almesberger@lrc.di.epfl.ch>
License: GPL-3+

Files: src/boot.h
Copyright: 2017, Andreas Bombe <aeb@debian.org>
 2008-2014, Daniel Baumann <mail@daniel-baumann.ch>
 1993, Werner Almesberger <werner.almesberger@lrc.di.epfl.ch>
License: GPL-3+

Files: src/charconv.c
 src/charconv.h
Copyright: 2018-2020, Pali Rohár <pali.rohar@gmail.com>
 2010, Alexander Korolkov <alexander.korolkov@gmail.com>
License: GPL-3+

Files: src/check.h
 src/common.h
 src/fat.h
 src/file.h
Copyright: 2008-2014, Daniel Baumann <mail@daniel-baumann.ch>
 1993, Werner Almesberger <werner.almesberger@lrc.di.epfl.ch>
License: GPL-3+

Files: src/common.c
 src/fat.c
 src/file.c
 src/fsck.fat.c
Copyright: 2018-2021, Pali Rohár <pali.rohar@gmail.com>
 2008-2014, Daniel Baumann <mail@daniel-baumann.ch>
 1998, Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>
 1993, Werner Almesberger <werner.almesberger@lrc.di.epfl.ch>
License: GPL-3+

Files: src/device_info.c
Copyright: 2018, Pali Rohár <pali.rohar@gmail.com>
 2015, Andreas Bombe <aeb@debian.org>
License: GPL-3+

Files: src/fatlabel.c
Copyright: 2017, 2018, Pali Rohár <pali.rohar@gmail.com>
 2015-2017, Andreas Bombe <aeb@debian.org>
 2008-2014, Daniel Baumann <mail@daniel-baumann.ch>
 2007, Red Hat, Inc.
 1998, Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>
 1993, Werner Almesberger <werner.almesberger@lrc.di.epfl.ch>
License: GPL-3+

Files: src/fsck.fat.h
 src/io.c
Copyright: 2015, Andreas Bombe <aeb@debian.org>
 2008-2014, Daniel Baumann <mail@daniel-baumann.ch>
 1998, Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>
 1993, Werner Almesberger <werner.almesberger@lrc.di.epfl.ch>
License: GPL-3+

Files: src/io.h
Copyright: 2008-2014, Daniel Baumann <mail@daniel-baumann.ch>
 1998, Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>
 1993, Werner Almesberger <werner.almesberger@lrc.di.epfl.ch>
License: GPL-3+

Files: src/lfn.c
Copyright: 2015, Andreas Bombe <aeb@debian.org>
 2008-2014, Daniel Baumann <mail@daniel-baumann.ch>
 1998, Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>
License: GPL-3+

Files: src/lfn.h
 src/version.h.in
Copyright: 2008-2014, Daniel Baumann <mail@daniel-baumann.ch>
 1998-2005, Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>
License: GPL-3+

Files: src/mkfs.fat.c
Copyright: 2018, Pali Rohár <pali.rohar@gmail.com>
 2015, 2016, Andreas Bombe <aeb@debian.org>
 2008-2014, Daniel Baumann <mail@daniel-baumann.ch>
 1998-2005, Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>
 1998, H. Peter Anvin <hpa@zytor.com>
 1993, 1994, David Hudson <dave@humbug.demon.co.uk>
 1992, 1993, Remy Card <card@masi.ibp.fr>
 1991, Linus Torvalds <torvalds@klaava.helsinki.fi>
License: GPL-3+

Files: tests/test-fsck
 tests/test-mkfs
Copyright: 2015, 2016, Andreas Bombe <aeb@debian.org>
License: GPL-3+

Files: tests/test-label
Copyright: 2018, Pali Rohár <pali.rohar@gmail.com>
License: GPL-3+

Files: ChangeLog Makefile.in tests/Makefile.in
Copyright: 1991 Linus Torvalds <torvalds@klaava.helsinki.fi>
 1992-1993 Remy Card <card@masi.ibp.fr>
 1993-1994 David Hudson <dave@humbug.demon.co.uk>
 1999-2005 Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>
 1999-2005 Werner Almesberger <werner.almesberger@lrc.di.epfl.ch>
 1999-2005 Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>
 2008-2014 Daniel Baumann <mail@daniel-baumann.ch>
 2015-2017 Andreas Bombe <aeb@debian.org>
 2017-2021 Pali Rohár <pali.rohar@gmail.com>
License: GPL-3+
